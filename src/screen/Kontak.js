import React from "react";
import { Image, StyleSheet } from "react-native";
import {
    Text,
    Container,
    Content,
    Button,
    Thumbnail,
    Grid,
    Row,
    Col,
    View
} from "native-base";
import { AppHeader, AppFooter } from "../app-nav/index";
import call from 'react-native-phone-call'

export default class Kontak extends React.Component {
    render() {
        return (
            <Container style={{ backgroundColor: "#FFF" }}>
                <AppHeader title="Telepon Penting" />
                <Content>
                    <Text style={styles.titleText}>
                    </Text>
                    <Text style={styles.titleText}>
                        Informasi Telepon Penting
                </Text>
                    <Grid>
                        <Row>
                            <Col style={{ height: 150, justifyContent: 'flex-end' }}>
                                <Button style={{ height: 100, width: 153, alignSelf: 'center' }} transparent onPress={this.callBencanaAlam}>
                                    <Image style={{ height: 100, width: 153 }} source={require("../../img/asset/bencana_alam.png")} />
                                </Button>
                            </Col>
                            <Col style={{ height: 150, justifyContent: 'flex-end' }}>
                                <Button style={{ height: 100, width: 153, alignSelf: 'center' }} transparent onPress={this.callPolisi}>
                                    <Image style={{ height: 100, width: 153 }} source={require("../../img/asset/kantor_polisi.png")} />
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{ height: 150, justifyContent: 'center' }}>
                                <Button style={{ height: 100, width: 153, alignSelf: 'center' }} transparent onPress={this.callPemadamKebakaran}>
                                    <Image style={{ height: 100, width: 153 }} source={require("../../img/asset/pemadam_kebakaran.png")} />
                                </Button>
                            </Col>
                            <Col style={{ height: 150, justifyContent: 'center' }}>
                                <Button style={{ height: 100, width: 153, alignSelf: 'center' }} transparent onPress={this.callAmbulan}>
                                    <Image style={{ height: 100, width: 153 }} source={require("../../img/asset/ambulan.png")} />
                                </Button>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
                <AppFooter navigation={this.props.navigation} />
            </Container>
        );
    }

    go(param) {
        if (param == "kontak") this.props.navigation.navigate("Kontak");
    }
    callPemadamKebakaran = () => {
        <Button onPress={call(pemadamKebakaran).catch(console.error)}></Button>
    }
    
    callAmbulan = () => {
        <Button onPress={call(ambulan).catch(console.error)} ></Button>
    }
    callPolisi = () => {
        <Button onPress={call(polisi).catch(console.error)}></Button>
    }
    callBencanaAlam = () => {
        <Button onPress={call(bencanaAlam).catch(console.error)} ></Button>
    }
};

const styles = StyleSheet.create({
    titleText: {
        fontFamily: 'Ubuntu_Regular',
        fontSize: 20,
        fontWeight: 'bold',
        marginLeft: "5%"
    }
}
)

const pemadamKebakaran = {
    number: '118', // String value with the number to call
    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}


const ambulan = {
    number: '113', // String value with the number to call
    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}
const polisi = {
    number: '110', // String value with the number to call
    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}


const bencanaAlam = {
    number: '129', // String value with the number to call
    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}

