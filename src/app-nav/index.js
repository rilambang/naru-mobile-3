import React from "react";
import { Image } from "react-native";
import {
  StyleProvider,
  Header,
  Left,
  Right,
  Body,
  Title,
  Button,
  Icon,
  Footer,
  FooterTab
} from "native-base";
import getTheme from "./../../native-base-theme/components/";
import customColor from "./../../native-base-theme/variables/customColor";

export class AppHeader extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const isHome = this.props.isHome;
    const title = this.props.title;
    const ImageHeader = props => (
      <View style={{ backgroundColor: '#eee' }}>
        <Image
          style={StyleSheet.absoluteFill}
          source={{ uri: '../../img/asset/teman-berbagi.png' }}
        />
        <Header {...props} style={{ backgroundColor: 'transparent' }}/>
      </View>
    );
    return (
        <Header style={{marginTop:'5%', backgroundColor: '#eee' }}>
          <Left>
            {
              !isHome ? (
                        <Button transparent
                        >
                            <Icon style={{ color: "#000" }}  name="arrow-back" />
                        </Button>
              ) : (
                <Image style={{width: 150}} source={require("../../img/asset/teman-berbagi.png")} resizeMode='contain' />
              )
            }
          </Left>
          <Body>
            { !isHome && (<Title style={{ color: 'black' }}>{title}</Title>) }
          </Body>
          <Right>
            <Button transparent>
              <Icon name='ios-bookmark-outline' style={{ color: 'black' }} />
            </Button>
            <Button transparent>
              <Icon name='ios-settings-outline' style={{ color: 'black' }} />
            </Button>
          </Right>
        </Header>
    );
  }
};

export class AppFooter extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const navigation = this.props.navigation;
    const routeName = navigation.state.routeName;
    
    return (
      <StyleProvider style={getTheme(customColor)}>
        <Footer>
          <FooterTab>
            <Button onPress={() => navigation.navigate("Home")} active={routeName == 'Home'}>
              <Icon name="ios-home-outline" />
            </Button>
            <Button onPress={() => navigation.navigate("PromoMenu")} active={routeName == 'PromoMenu' || routeName == 'Promo'}>
              <Icon name="ios-ribbon-outline" />
            </Button>
            <Button onPress={() => navigation.navigate("News")} active={routeName == 'News'}>
              <Icon name="ios-paper-outline" />
            </Button>
            <Button onPress={() => navigation.navigate("Peta")} active={routeName == 'Peta'}>
              <Icon name="ios-pin-outline" />
            </Button>
            <Button onPress={() => navigation.navigate("Kontak")} active={routeName == 'Kontak'}>
              <Icon name="ios-call-outline" />
            </Button>
          </FooterTab>
        </Footer>
      </StyleProvider>
    );
  }
};